// Assignment 4
/* 
    Write a JavaScript program which accept a string as input and swap the case of each character. 
    For example if you input The Quick Brown Fox the output should be tHE qUICK bROWN fOX.
*/
function charReverseHandler (string) {
    const array = string.split(" ")
        for(let i = 0; i < array.length; i++) {
            array[i] = array[i].charAt(0).toLowerCase() + array[i].slice(1).toUpperCase();
        }
        const string2 = array.join(" ")
        return string2;
};
// console.log(charReverseHandler("Hello islom numanov from Udevs"));
//------------------------------------------
//------------------------------------------
/* 
    2 Write a JavaScript program to add items in an _blank array and display the items. Use CSS if it is neccessary
*/
/* var list, todo, newText, newLi, button;
    list = document.querySelectorAll('li');
    todo = document.getElementById('todo');
    newText = document.getElementById('newText');
    button = document.getElementById('button')
function submitOnEnter(event) {
	if(event.keyCode === 13){
		button.click();
	}
}
function add () {
	if(newText.value.length>0){
		newLi = '<li onclick="toggleLineTrough(this)"  style="user-select:none;"><span style="text-decoration:none">' + newText.value + '</span><span>&nbsp;&nbsp;&nbsp;</span><span class="delete" onclick="deleteTodo(this)"></span></li>'
		todo.innerHTML+= newLi;
		newText.value = "";
	}
}

function toggleLineTrough(x) {
	if (x.firstChild.style.textDecoration=='none'){
		x.firstChild.style.textDecoration='line-through';
		x.lastChild.innerHTML = "🗑️";
	}else if(x.firstChild.style.textDecoration=='line-through'){
		x.firstChild.style.textDecoration='none';
		x.lastChild.innerHTML = "";
	}
}
function deleteTodo(y) {
	y.parentNode.parentNode.removeChild(y.parentNode)
} */
//------------------------------------------
//------------------------------------------
// 3 Write a JavaScript program to find duplicate values in a JavaScript array.
function findDublicatedHandler () {
    const array = [1, 1, 2, 99, 3, 4, 5, 5, 1555, 99, 1555]
    let duplicate = [];
    const tempArray = [...array].sort();
    for(let i = 0; i<tempArray.length; i++){
        if(tempArray[i + 1] === tempArray[i]){
            duplicate.push(tempArray[i])
        }
    }
    return duplicate;
}
// console.log(findDublicatedHandler()); 
//------------------------------------------
//------------------------------------------
/* // 4 Write a JavaScript program to compute the union of two arrays. 
Example: 
input => [1,2,3,4,5], [1,a,f,3,4,5] 
output =>  [1,2,3,4,5,a,f] */
function unionHandler2 (arr1,arr2) {
   return Array.from(new Set(arr1.concat(arr2)))
};
console.log(unionHandler2([1,2,3,4,5], [1,"a","f",3,4,5]));
//------------------------------------------
//------------------------------------------
/* 
    5 Write a JavaScript function to remove. 'null', '0', '""', 'false', 'undefined' and 'NaN' values from an array
*/
function unionHandler () {
    let falsyArray = [false, true, 0, "0", null, undefined, ""]
    let truthArray = falsyArray.filter(Boolean);
    return truthArray;
};
// console.log(unionHandler([false, true, 0, "0", null, undefined]));
//------------------------------------------
//------------------------------------------
/* 
    6 Write a JavaScript function that returns a passed string with letters in alphabetical order
*/
let string = "AAcbL";
console.log((string.split("").sort(reverseStringHandler).join("")));
function reverseStringHandler (a, b) {
    let result = 0;
    a = a.toLowerCase();
    b = b.toLowerCase();
    if(a > b) result = 1;
    if(a < b) result = -1;
    return result;
};